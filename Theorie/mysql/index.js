const express = require("express");
const mysql = require("mysql");

const app = express();
app.use(express.urlencoded({extended: false}));
app.use(express.json());

const pool = mysql.createPool({database: "theorie", password: "webprog", user: "root", host: "localhost", connectionLimit: 10});

app.get("/", (req, res) => {
    pool.getConnection( (err, conn) => {

        if(err)
            throw err;

        conn.query("SELECT * FROM `les`", (err, rows) => {
            conn.release();

            if(err)
                throw err;

            res.send(rows);
        });

    });

});

app.listen(3000, err => {
    if(err)
        console.log(err);

    console.log("Running on port 3000");
});
