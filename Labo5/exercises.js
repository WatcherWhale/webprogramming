function onClick(selector, handler)
{
    document.querySelector(selector).addEventListener("click", handler);
}

document.addEventListener("DOMContentLoaded", e => {
    // Exercise 1
    onClick("#e1b", e => {
        const div = document.getElementById("e1d");
        div.classList.toggle("selected");
    });


    // Exercise 2
    onClick("#e2b", e => {
        const div = document.getElementById("e1d");

        div.innerHTML = "Changed";
    });


    // Exercise 3
    let divCounter = 0;
    onClick("#e3b", e => {

        const d1 = document.getElementById("e3d1");
        const d2 = document.getElementById("e3d2");
        const d3 = document.getElementById("e3d3");
        const divs = [d1, d2, d3];

        divCounter = (divCounter + 1) % 4;

        for(const i in divs)
        {
            divs[i].classList.toggle("selected", false);
        }

        if(divCounter >= 0)
        {
            divs[divCounter - 1].classList.toggle("selected", true);
        }
    });


    onClick("#e4b", e => {
        const table = document.getElementById("e4t").children[0];

        for(const r in table.children)
        {
            const tr = table.children[r];

            for(const c in tr.children)
            {
                const td = tr.children[c];
                const value = parseInt(td.textContent);

                if(value > 10)
                {
                    td.classList.toggle("selected", true);
                }
            }
        }
    });



    const MOUNTAINS = [ 
       {name: "Kilimanjaro", height: 5895, place: "Tanzania"}, 
       {name: "Everest", height: 8848, place: "Nepal"}, 
       {name: "Mount Fuji", height: 3776, place: "Japan"}, 
       {name: "Vaalserberg", height: 323, place: "Netherlands"}, 
       {name: "Denali", height: 6168, place: "UnitedStates"}, 
       {name: "Popocatepetl", height: 5465, place: "Mexico"}, 
       {name: "Mont Blanc", height: 4808, place: "Italy/France"} 
    ];

    let parent = document.getElementById("ee1d");
    let table = document.createElement("table");
    parent.append(table)

    table.border = "1px";
    table.innerHTML = "<tr><th>name</th><th>height</th><th>place</th></tr>";

    for(const m in MOUNTAINS)
    {
        const mountain = MOUNTAINS[m];
        const row = document.createElement("tr");
        table.append(row);

        row.innerHTML = "<td>" + mountain.name + "</td><td>" + mountain.height + "</td><td>" + mountain.place + "</td>";
    }


    window.addEventListener("keydown", e => {
        const balloon = document.getElementById("ee2d");
        let size = balloon.style.fontSize;
        console.log(size)
        size = parseFloat(size.substr(0, size.length - 2));
        console.log(size)

        if(e.keyCode == 39)
        {
            size *= 1.1;
        }
        else if(e.keyCode == 37)
        {
            size *= 0.9;
        }

        console.log(size)

        if(size < 5) size = 5;
        balloon.style.fontSize = size + "px";

        if(size > 50) balloon.innerHTML = "💥";
        else balloon.innerHTML = "🎈";
    });






















});
