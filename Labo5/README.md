# Lab 5
## Oefening 1:
Maak een script die de achtergrond van een div laat veranderen na het klikken op de knop Verander.

![](images/images-000.png)
![](images/images-001.png)


Wanneer men terug op de knop “Verander” klikt moet de achtergrondkleur terug verdwenen zijn.

## Oefening 2:
Maak een script die de tekst van een div laat veranderen na het klikken op de knop Verander.

![](images/images-002.png)
![](images/images-003.png)

## Oefening 3:
Zet 3 divs onder elkaar, en maak een script aan om de achtergrondkleur van 1 div telkens te veranderen
na een klik op de knop. Dus achtergrond 1 zal veranderen na het klikken op de knop. Wanneer men
opnieuw klikt verdwijnt de achtergrond van 1 en heeft 2 een achtergrond kleur, enzovoort.

![](images/images-004.png)

![](images/images-005.png)
![](images/images-006.png)
![](images/images-007.png)

## Oefening 4:

Maak een tabel aan in HTML:

![](images/images-008.png)

Schrijf een script dat de rijen en kolommen uitleest en elk getal groter dan 10 een rode achtergrondkleur
geeft.

![](images/images-009.png)

## Oefening 5:
Schrijf met behulp van een javascript functie een webpagina waarin je de voornaam van een klasgenoot
moet typen waarbij de functie het geboortejaar teruggeeft. Maak gebruik van arrays om te zoeken!
Tip: maak 1 array waarin je de namen van klasgenoten zet, en een tweede array waarin je geboortejaren plaatst. (Ga van de
veronderstelling uit dat de positie van naam in de ene array overeenkomt met de positie van het geboortejaar van de andere array)

## Oefening 6:
Maak een script waarin je 1000 keer willekeurige getallen genereert tussen 1 en 12 (ogen van een
dobbelsteen).
De 1000 resultaten neem je op in een array. Je maakt ook een array van 11 elementen waarin je noteert
hoeveel maal de computer het getal 2,3,4,...12 heeft gegenereerd. Deze laatste array print je op je html
pagina.

# Expert
## Oefening 1:
Bouw via code automatisch een HTML tabel als volgt:

```html
<table>
    <tr>
        <th>name</th>
        <th>height</th>
        <th>place</th>
    </tr>
    <tr>
       <td>Kilimanjaro</td>
       <td>5895</td>
       <td>Tanzania</td>
    </tr>
</table>
```

Voor elke rij de `<table>` tag bevat een `<tr>` tag. Binnen de `<tr>` tag zetten we cell elementen: dit is ofwel een heading cell `<th>` of een gewone cell `<td>`

Je krijgt een dataset van bergen. Dit is een array van objecten met name, height, en place eigenschappen. Ga bij het laden van het document automatisch over deze dataset en genereer de tabel.

De dataset krijg je:

```javascript
const MOUNTAINS = [ 
    {name: "Kilimanjaro", height: 5895, place: "Tanzania"}, 
    {name: "Everest", height: 8848, place: "Nepal"}, 
    {name: "Mount Fuji", height: 3776, place: "Japan"}, 
    {name: "Vaalserberg", height: 323, place: "Netherlands"}, 
    {name: "Denali", height: 6168, place: "UnitedStates"}, 
    {name: "Popocatepetl", height: 5465, place: "Mexico"}, 
    {name: "Mont Blanc", height: 4808, place: "Italy/France"} 
 ];
```

## Oefening 2:

Maak een pagina die een ballon toont (maak gebruik van de balloon emoji : [🎈 Balloon Emoji (emojipedia.org)](https://emojipedia.org/balloon/) ).

Schrijf een programma zodat de browser detecteert dat je op de "up-arrow" of "down-arrow" toets drukt. Wanneer je vb. op de "up" toets klikt ga je de ballon automatisch 10% groter maken, en op de "down" arrow 10% kleiner.

Zorg ervoor dat de balloon emoji binnen een parent element zit. Dan kan je de grootte bepalen door gebruik te maken van de font-size eigenschap. (via javascript : style.fontSize). Hou rekening dat je steeds een unit meegeeft: vb. 15px.

Deel 2 van deze oefening: wanneer de fontSize bijvoorbeeld groter is dan 50pxd dan toon je een explosie emoji.
