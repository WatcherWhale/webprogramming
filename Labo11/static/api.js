window.addEventListener("DOMContentLoaded", (e) => {
    const trips = doRequest("trips", "get");
    const table = document.getElementById("trips");

    for(const i in trips)
    {
        const trip = trips[i];

        const row = document.createElement("tr");

        const id = document.createElement("td");
        id.innerText = trip.id;
        const name = document.createElement("td");
        name.innerText = trip.name;
        const cost = document.createElement("td");
        cost.innerText = trip.cost;

        row.appendChild(id);
        row.appendChild(name);
        row.appendChild(cost);
        table.appendChild(row);
    }

});

function doRequest(path, method)
{
    const xhr = new XMLHttpRequest();
    xhr.open(method, window.location.href + path, false);

    xhr.send();

    if(xhr.status != 200) return null;

    return JSON.parse(xhr.response);
}
