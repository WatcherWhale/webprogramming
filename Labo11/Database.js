const mysql = require('mysql');

class Database
{
    constructor()
    {
        this.pool = mysql.createPool({host: "127.0.0.1", user: "root", password: "webprog", database: "travel"});
    }

    addTrip(name)
    {
        return this.doQuery("INSERT INTO `trips`(`name`) VALUES ('" + name + "')");
    }

    editTrip(id, cost)
    {
        return this.doQuery("UPDATE `trips` SET `cost`='" + cost + "' WHERE `id`=" + id);
    }

    gettrip(id)
    {
        return this.doQuery("SELECT * FROM `trips` WHERE `id`=" + id);
    }

    getAllTrips()
    {
        return this.doQuery("SELECT * FROM `trips`");
    }

    async getSum()
    {
        let sum = 0;
        const rows = await this.getAllTrips();

        for(const i in rows)
        {
            const row = rows[i];

            sum += row.cost;
        }

        return sum;
    }

    doQuery(query)
    {
        return new Promise((resolve, reject) =>
        {
            this.pool.getConnection((err, conn) =>
            {
                if(err)
                {
                    reject(err);
                    return;
                }

                conn.query(query, (err, rows) => 
                {
                    conn.release();

                    if(err)
                    {
                        reject(err);
                        return;
                    }

                    resolve(rows);
                });
            });
        });
    }
}

module.exports = Database;
