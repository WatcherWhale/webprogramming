const express = require('express');
const Database = require('./Database');

const tripsRouter = require('./tripsRouter');

const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.static("static"));
app.use("/trips", tripsRouter);

const db = new Database();

app.get("/costs", async (req, res) => {
    res.header('Content-Type', 'application/json')
        .send({"totalCost": await db.getSum()});
});

app.listen(3001);
