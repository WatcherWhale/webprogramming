const express = require('express');
const router = express.Router();

const Database = require('./Database');
const db = new Database();

router.route("/")
    .get(async (req, res) => {
        res.header('Content-Type', 'application/json')
            .send(await db.getAllTrips());
    })
    .put(async (req, res) => {
        await db.addTrip(req.body.name);

        res.header('Content-Type', 'application/json')
            .send({"status": "OK"});
    });

router.route("/:id")
    .post(async (req, res) => {
        await db.editTrip(req.params.id, req.body.cost);

        res.header('Content-Type', 'application/json')
            .send({"status": "OK"});
    })
    .get(async (req, res) => {
        res.header('Content-Type', 'application/json')
            .send((await db.gettrip(req.params.id))[0]);
    });

module.exports = router;
