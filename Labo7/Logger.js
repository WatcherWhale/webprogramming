class Logger
{
    info(msg)
    {
        console.log("[INFO] " + msg);
    }

    warning(msg)
    {
        console.log("[WARN] " + msg);
    }

    error(msg)
    {
        console.log("[ERROR] " + msg);
    }
}

module.exports.Logger = Logger;
