const Game = require('./Game');
const Scoreboard = require('./Scoreboard');
const User = require('./User');

const board = new Scoreboard();
const game = new Game();

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

rl.question("What's your name? ", (name) => {
    rl.question("What's your first name? ", (firstName) => {
        const user = new User(firstName, name);

        rl.question("How many questions do you want? ", (amount) => {
            askQuestion(user, parseInt(amount) - 1);
        });
    });
});


function askQuestion(user, amountLeft)
{
    const question = game.generateQuestion();

    rl.question(question.x + " + " + question.y + " = ", (answer) => {

        if(game.validateQuestion(question, answer))
        {
            board.addPoint(user);
        }

        if(amountLeft > 0)
        {
            askQuestion(user, amountLeft - 1);
        }
        else
        {
            board.displayScore(user);
            rl.close();
        }
    });
}
