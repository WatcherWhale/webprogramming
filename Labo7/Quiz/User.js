class User
{
    constructor(firstName, name)
    {
        this.firstName = firstName;
        this.name = name;
        this.id = this.firstName + this.name;
    }
}

module.exports = User;
