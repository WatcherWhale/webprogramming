class Scoreboard
{
    constructor()
    {
        this.scores = {};
    }

    addPoint(user)
    {
        let obj = this.getScoreObj(user);
        obj.score += 1;
        obj.max += 1;

        this.updateScore(user, obj);
    }

    updateScore(user, scoreObj)
    {
        this.scores[user.id] = scoreObj;
    }

    getScoreObj(user)
    {
        if(Object.keys(this.scores).indexOf(user.id) !== -1)
        {
            return this.scores[user.id];
        }

        return {score: 0, max: 0};
    }

    displayScore(user)
    {
        const score = this.getScoreObj(user);
        console.log(user.firstName + " scored " + score.score + " / " + score.max);
    }
}

module.exports = Scoreboard
