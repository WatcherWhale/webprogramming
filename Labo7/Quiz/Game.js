class Game
{
    constructor(min = 0, max = 10)
    {
        this.min = min;
        this.max = max;
    }

    generateRandom()
    {
        return Math.round(Math.random() * (this.max - this.min) + this.min);
    }

    generateQuestion()
    {
        const x = this.generateRandom();
        const y = this.generateRandom();

        return { x: x, y: y };
    }

    validateQuestion(question, answer)
    {
        return question.x + question.y === parseInt(answer);
    }
}

module.exports = Game;
