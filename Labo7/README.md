# Lab 7

## Node 1

1. Maak een javascript bestand aan: vb. test.js (let op de .js extensie)
2. schrijf in je javascript bestand: `console.log("test")`

3. Voor de javascript file uit door naar de terminal te gaan, en naar de juiste folder te navigeren.

Daarna typ je in de terminal: `node test.js`

## Node 2

### Oefening 1:

Maak een logger klasse waarbij je info, warning en error berichten (zijn methoden van de klasse) kan versturen. Zet deze klasse in een module die je in andere bestanden kan aanspreken.

### Oefening 2:
Installeer de cowsay module, en gebruik deze in je index.js bestand als module.
[https://www.npmjs.com/package/cowsay](https://www.npmjs.com/package/cowsay)

## Node 3

Converteer deze CSV file naar JSON objecten.

```
first_name;last_name;email
Tom;Peeters;tom.peeters@ap.be
Tim;Dams;tim.dams@ap.be
Test;Test;Test@test.be
```

** Ga zelf op zoek naar een npm package die csv naar json kan converteren.

## Node 4

### Oefening 1

Importeer the 3rd party lodash klasse, en genereer hiermee een random getal tussen 50 en 100.

### Oefening 2

```javascript
function add(x, y) {
  return x + y;
}

function subtract(x, y) {
  return x - y;
}
```

Gebruik deze functies in andere javascript bestanden.

### Oefening 3

Maak een console applicatie aan in Nodejs waarbij je aan de gebruiker vraagt om een reeks sommen te maken.
Ons systeem zal vervolgens de sommen controleren en een score teruggeven. 

Requirements : vraag aan de gebruiker naam en voornaam, voor welk getal hij sommen wil oplossen en een hoeveelheid sommen.


Bijvoorbeeld: (tussen [ ] zijn de antwoorden in de console van de gebruiker)

```
What's your name? [Peeters]

What's your firstname? [Tom]

Over welk getal wil je sommen oplossen? [5]

Hoeveel sommen wil je oplossen? [2]

0 + 5 = [5]

1 + 5 = [2]

Best Tom, je behaalde : 1 / 2
```

Om dit systeem te ontwikkelen bouw je een user klasse, game klasse en scoreboard klasse. Dit zijn vervolgens ook allemaal modules.

In de user klasse bewaar je een naam en voornaam, de game klasse bevat de logica om je sommen aan te maken, en je antwoorden te vergelijken met de juiste antwoorden (hiervoor kan misschien de eval functie kunnen helpen?). De scoreboard klasse zorgt er voor dat je score op deze manier in de console terecht komt: 
```
Best Tom, je behaalde : 1 / 2
```
