const Logger = require("./Logger").Logger;
const math = require('./math');

const logger = new Logger();
logger.info("This is an info message.");
logger.warning("This is a warning message.");
logger.error("This is an error message.");

logger.info(math.add(5, 7));
logger.info(math.subtract(5, 7));
