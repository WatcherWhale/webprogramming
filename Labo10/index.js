const express = require('express');
const Database = require('./Database');

const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.json());

const db = new Database();


app.put("/trips", async (req, res) => {
    await db.addTrip(req.body.name);

    res.header('Content-Type', 'application/json')
        .send({"status": "OK"});
});

app.post("/trips/:id", async (req, res) => {
    await db.editTrip(req.params.id, req.body.cost);

    res.header('Content-Type', 'application/json')
        .send({"status": "OK"});
});

app.get("/trips/:id", async (req, res) => {
    res.header('Content-Type', 'application/json')
        .send((await db.gettrip(req.params.id))[0]);
});

app.get("/trips", async (req, res) => {
    res.header('Content-Type', 'application/json')
        .send(await db.getAllTrips());
});

app.get("/costs", async (req, res) => {
    res.header('Content-Type', 'application/json')
        .send({"totalCost": await db.getSum()});
});

app.listen(3001);
