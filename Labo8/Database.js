class Database
{
    constructor()
    {
        this.trips = {};
    }

    addTrip(id, cost)
    {
        this.trips[id] = {"cost": cost};
    }

    gettrip(id)
    {
        if(id in this.trips)
            return this.trips[id];
        else
            return {};
    }

    getAllTrips()
    {
        return this.trips;
    }

    getSum()
    {
        let sum = 0;

        const keys = Object.keys(this.trips);
        for(const i in keys)
        {
            const id = keys[i];
            sum += this.trips[id].cost;
        }

        return sum;
    }

}

module.exports = Database;
