const express = require('express');
const Database = require('./Database');

const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.json());

const db = new Database();


app.post("/trips/:id", (req, res) => {
    db.addTrip(req.params.id, req.body.cost)
    res.send("ok");
});

app.get("/trips/:id", (req, res) => {
    res.send(JSON.stringify(db.gettrip(req.params.id)));
});

app.get("/trips", (req, res) => {
    res.header('Content-Type', 'application/json')
        .send(db.getAllTrips());
});

app.get("/costs", (req, res) => {
    res.header('Content-Type', 'application/json')
        .send({"totalCost": db.getSum()});
});

app.listen(3001);
