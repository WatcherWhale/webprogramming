class GameObject
{
    constructor()
    {
        this.x = 0;
        this.y = 0;

        this.width = 0;
        this.height = 0;

        this.color = "#000";
        this.tag = "GameObject"
    }

    update(game)
    {

    }

    draw(ctx)
    {
        ctx.beginPath();
        ctx.fillStyle = this.color;
    }

    isColliding(gameObject)
    {

    }

    constrainX(x)
    {
        if(x < 0) return 0;
        if(x > 500 - this.width) return 500 - this.width;
        return x
    }

    constrainY(x)
    {
        if(x < 0) return 0;
        if(x > 500 - this.height) return 500 - this.height;
        return x
    }

    getTop()
    {
        return {x: this.x, y: this.y};
    }

    getBottom()
    {
        return {x: this.x + this.width, y: this.y + this.height};
    }

    getTag()
    {
        return this.tag;
    }
}

class Player extends GameObject
{
    constructor()
    {
        super();

        this.x = 10;

        this.width = 10;
        this.height = 50;

        this.tag = "Player";

    }

    draw(ctx)
    {
        super.draw(ctx);
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    update(game)
    {
        super.update(game);

        this.y = this.constrainY(game.mouse.y - this.height/2);
    }
}

class Enemy extends GameObject
{
    constructor()
    {
        super();

        this.x = 480;

        this.width = 10;
        this.height = 50;

        this.tag = "Enemy";

    }

    draw(ctx)
    {
        super.draw(ctx);
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    update(game)
    {
        super.update(game);

        this.y = this.constrainY(game.ball.y - this.height/2);
    }
}

class Ball extends GameObject
{
    constructor(canvas)
    {
        super();

        this.x = canvas.width / 2;
        this.y = canvas.height / 2;

        this.width = 5;
        this.height = 5;

        this.speed = 5;
        this.direction = {x: -1, y: 0.7};
    }

    update(game)
    {
        super.update(game);
        this.x = this.constrainX(this.x + this.direction.x * this.speed);
        this.y = this.constrainY(this.y + this.direction.y * this.speed);

        if(this.x <= this.width)
        {
            this.direction.x *= -1;
            // Point enemy
        }
        else if(this.x >= 500 - this.width)
        {
            this.direction.x *= -1;
            // Player Point
        }

        if(this.y <= this.width || this.y >= 500 - this.width)
        {
            this.direction.y *= -1;
        }
    }

    draw(ctx)
    {
        super.draw(ctx);
        ctx.arc(this.x, this.y, this.width * 2, 0, 2*Math.PI);
        ctx.fill();
    }

    getTop()
    {
        return {x: this.x - this.width, y: this.y - this.width};
    }

    getBottom()
    {
        return {x: this.x + this.width, y: this.y + this.width};
    }

    isColliding(gameObject)
    {
        const top = this.getTop();
        const bottom = this.getBottom();
        const gTop = gameObject.getTop();
        const gBottom = gameObject.getBottom();

        const xDist = Math.abs(top.x - gTop.x);

        if(xDist <= this.width && this.y >= gTop.y && this.y <= gBottom.y)
        {
            if(gameObject.getTag() == "Player")
            {
                this.x = gBottom.x - this.direction.x * 20
                this.direction.x *= -1;
            }
            else if(gameObject.getTag() == "Enemy")
            {
                this.x = gTop.x - this.direction.x * 20
                this.direction.x *= -1;
            }
        }
    }
}
