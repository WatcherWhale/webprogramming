document.addEventListener("DOMContentLoaded", onLoad);

function onLoad(e)
{
    const game = new Game(document.getElementById("gameWindow"));

    document.addEventListener("mousemove", e => game.updateMousePosition(e.x, e.y));

    game.start();
}

class Game
{
    static instance;

    constructor(canvas)
    {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');

        this.mouse = {x:0, y:0};
        this.ball = new Ball(this.canvas)

        this.gameObjects = [ new Player(), this.ball, new Enemy()];

        Game.instance = this;
    }

    start()
    {
        Game.gameLoop();
    }

    clear()
    {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    update()
    {
        this.gameObjects.forEach(obj => {
            obj.update(this);
            this.gameObjects.forEach(other => obj.isColliding(other));
        });
    }

    draw()
    {
        this.ctx.beginPath();
        this.ctx.rect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.stroke();

        this.gameObjects.forEach(obj => obj.draw(this.ctx));
    }

    updateMousePosition(x, y)
    {
        const rect = this.canvas.getBoundingClientRect();
        this.mouse.x = x - rect.x;
        this.mouse.y = y - rect.y;
    }

    getDimensions()
    {
        return {width: this.canvas.width, height: this.canvas.height}
    }

    static gameLoop()
    {
        Game.instance.clear();
        Game.instance.update()
        Game.instance.draw()

        setTimeout(Game.gameLoop, 33);
    }
}



