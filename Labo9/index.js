const express = require("express");

const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());

let list = [];
let done = [];

app.get("/", (req, res) => {
    res.header('Content-Type', 'application/json')
        .send(list);
});

app.get("/done", (req, res) => {
    res.header('Content-Type', 'application/json')
        .send(done);
});

app.put("/add", (req, res) => {
    list.push(req.body);
});

app.delete("/del/:id", (req, res) => {
    const toDelete = list.find(i => i.id == req.params.id);
    const i = list.findIndex(toDelete);
    done.push(list.splice(i)[0]);
});

app.listen(8080);
