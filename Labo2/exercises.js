// Oefening 1

const print = (func) => {
    //const value = prompt("Geef een woord in.");
    const value = "Test";
    func(value);
}

console.log("Ex1");
print(console.log)

// Oefening 2

const min = (num1, num2) => {
    return Math.min(num1, num2);
}

console.log("Ex2");
console.log(min(10,100))

// Oefening 3

const countVowels = (str) => {

    const vowels = ['a','e','i','o', 'u'];
    let count = 0;

    str = str.toLowerCase();

    for(let i in str)
    {
        if(vowels.indexOf(str[i]) !== -1)
            count++;
    }

    return count;
}

console.log("Ex3");
console.log(countVowels("Hello WOrld!"));

// Oefening 4

const countBs = (str) => {

    let count = 0;
    str = str.toLowerCase();

    for(let i in str)
    {
        if(str[i] == "b") count++;
    }

    return count;
}

const countChar = (str, char) => {

    let count = 0;
    str = str.toLowerCase();

    for(let i in str)
    {
        if(str[i] == char) count++;
    }

    return count;
}

console.log("Ex4");
console.log(countBs("BBC")); // → 2
console.log(countChar("kakkerlak", "k")); // → 4

module.exports.min = min;
module.exports.countVowels = countVowels;
module.exports.countBs = countBs;
module.exports.countChar = countChar;
