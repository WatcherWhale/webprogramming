const exercises = require('../exercises.js');
const qunit = require("qunit");


qunit.test("Exercise 2", assert => {
    assert.equal(exercises.min(10,100), 10);
    assert.equal(exercises.min(100,10), 10);
});

qunit.test("Exercise 3", assert => {
    assert.equal(exercises.countVowels("aeiou"), 5);
    assert.equal(exercises.countVowels("hElLo WOrld"), 3);
});

qunit.test("Exercise 4", assert => {
    assert.equal(exercises.countBs("bBc"), 2);
    assert.equal(exercises.countChar("Kakkerlak", "k"), 4);
});
