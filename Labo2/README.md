# Labo 2

## Oefening 1:

Probeer de oefening om te vormen zodat de print functie niet afhankelijk is van de console.log output, maar dat je bijvoorbeeld dynamisch ook naar een alert bericht het gevraagde woord kan sturen.
```
function print() {
    const value = prompt("geef een woord in");
    console.log(value)
}
```

## Oefening 2:

Schrijf een functie die 2 argumenten meekrijgt en het kleinste getal teruggeeft. (vb. `console.log(min(0,10)) //output 0`)

## Oefening 3:

* `charAt` retourneert het karakter van de gespecificeerde positie.
* `length` retourneert de lengte van een string

1. Maak een functie die het aantal klinkers van een bepaald woord telt

2. Maak een functie die de eerste letter van een woord in een hoofdletter zet (toUpperCase())

## oefening 4: Bean counting

You can get the Nth character, or letter, from a string by writing `"string"[N]`. The returned value will be a string containing only one character (for example, `b`). The first character has position `0`, which causes the last one to be found at position `string.length - 1`. In other words, a two-character string has length `2`, and its characters have positions `0` and `1`.

Write a function `countBs` that takes a string as its only argument and returns a number that indicates how many uppercase `“B”` characters there are in the string.

Next, write a function called countChar that behaves like countBs, except it takes a second argument that indicates the character that is to be counted (rather than counting only uppercase “B” characters). Rewrite countBs to make use of this new function.

Run following code:
```
console.log(countBs("BBC")); // → 2
console.log(countChar("kakkerlak", "k")); // → 4
```

## Volgende Week: Oefening 5
