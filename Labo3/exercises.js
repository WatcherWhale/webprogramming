// Exercise 1

const names = ["A", "B", "C"];
const birthdays = ["10/10/10", "20/20/20", "0/0/0"];

function getBirthday(name)
{
    const index = names.indexOf(name);
    if(index == -1) return undefined;

    return birthdays[index];
}

console.log(getBirthday("A"));
console.log(getBirthday("B"));
console.log(getBirthday("C"));

// Exercise 2
let todoList = [];

function addTodoItem(item)
{
    todoList.push(item);
}

function removeFirstTodo()
{
    return todoList.shift();
}

function promoteTodo(index)
{
    const item = todoList.splice(index, 1)[0];
    todoList.unshift(item);
}

addTodoItem("1");
addTodoItem("2");
addTodoItem("3");

console.log(todoList);

removeFirstTodo();
console.log(todoList);

promoteTodo(1);
console.log(todoList);

// Exercise 3

const numArray = [10, 50, 5, 60, 100, 2, 555];

function getMinValues(arr)
{
    const sorted = arr.sort((a, b) => a > b);
    return sorted.splice(0, 2);
}

console.log(getMinValues(numArray));

// Exercise 4

function throwDice()
{
    let arr = [];

    for(let i = 0; i < 1000; i++)
    {
        const diceThrow = Math.round(Math.random() * 5 + 1) + Math.round(Math.random() * 5 + 1);
        arr.push(diceThrow);
    }

    return arr;
}

function generateHistogram(arr)
{
    let hist = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for(let i in arr)
    {
        const element = arr[i];
        hist[element - 2] = hist[element - 2] + 1;
    }

    return hist;
}

const throws = throwDice();
const hist = generateHistogram(throws);

console.log(hist);

// Exercise 5
function capitalise(str)
{
    const words = str.split(" ");

    for(let i in words)
    {
        words[i] = words[i][0].toUpperCase() + words[i].substr(1);
    }

    return words.join(" ");
}

console.log(capitalise("hey Dit is een wereld VOL JS. a"));
