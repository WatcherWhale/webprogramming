# Labo 1
## Exercices

1.Schrijf een script dat een random getal tussen 50 en 100 toont. Doe dit zolang de gebruiker nee ( of n ) in je prompt venster invult

2. Breng de tafel van vermenigvuldiging voor het getal 10 op het scherm. ( in tabel vorm), en geef de even en oneven rijen andere kleuren.

3.
•Ontwikkel een tekst adventure game
•We maken een scenario waar we tegen een zombie vechten. Vooraleer je vecht kies je een wapen (bijvoorbeeld een pijl & boog, bijl of een kip). Daarna start je het gevecht.
•Deel 1: Ontwikkel je spel zodat je 50% kans hebt om te winnen ( denk aan Math.random()).
•Deel 2: tracht je webpagina attractief te maken en afhankelijk van het wapen stijgen je kansen tot winst.

4.  De gebruiker geeft een getal in tussen 1 en 10 (via prompt). Het programma plaatst op de eerste regel 1 sterretje, op de tweede regel 2 sterretjes, ... Het aantal regels wordt aangegeven door het getal dat de gebruiker heeft opgegeven. Indien de gebruiker een getal ingeeft kleiner dan 1 of groter dan 10 krijgt hij een foutboodschap.

5.
a/ Maak een script dat alle cijfers van 1 tot 20 schrijft per 5 in 1 rij. Output het resultaat in een HTML tabel met een border van 1.
b/ Alle even getallen krijgen een bepaalde kleur, en de oneven getallen een andere.

6. Maak een script waarin een gegeven woord (vb. hogeschool) vertikaal getoond wordt (alle letters onder elkaar).

7. Maak een script die een gegeven woord op elk van de volgende manieren schrijft:
brussel
brusse
bruss
brus
bru
br
b
