"use strict";

// Exercise 1

function randomGenerator()
{
    let more;

    while (more != "nee" && more != "n")
    {
        const rand = Math.round(50 * Math.random() + 50);
        document.write(rand + "<br/>");

        more = prompt("Do you want to continue?");
    }
}

// Exercise 2
function multiplicationTables()
{
    document.write("<table border='1'>");

    for(let i = 0; i <= 10; i++)
    {
        const val = i * 10;

        if(i%2 == 0)
            document.write("<tr style='background-color: blue'><td>" + i + " x 10</td><td>" + val +  "</td></tr>");
        else
            document.write("<tr style='background-color: red'><td>" + i + " x 10</td><td>" + val +  "</td></tr>");
    }

    document.write("</table>")
}

// Exercise 3
function textGame(randomMode=true)
{
    const weapon = chooseWeapon();

    if(randomMode)
    {
        const won = Math.random() <= 0.5;

        if(won)
        {
            document.write("You won the game");
        }
        else
        {
            document.write("You lost the game");
        }
    }
    else
    {
        switch(weapon)
        {
            case 0:
                document.write("You missed<br/>Your enemy inflicted 999999 damage<br/>You died");
                break;
            case 1:
                document.write("You waived your axe.<br/>You inflicted 100 damage<br/>Your enemy inflicted 999999 damage<br/>You died!")
                break;
            case 2:
                document.write("The chicken went beserk!<br/>Your enemy died!<br/>You died!");
                break;
        }
    }
}

function chooseWeapon()
{
    let option = -1;

    do
    {
        option = parseInt(prompt("0: Bow, 1: Axe, 2: Chicken"));
    } while(option >= 3 && option < 0);

    return option;
}

// Exercise 4
function printLines()
{
    const option = parseInt(prompt("Number between 1-10"));

    if(option > 0 && option <= 10)
    {
        for(let i = 0; i < option; i++)
        {
            let str = "";

            for(let j = 0; j <= i; j++)
            {
                str += "*";
            }

            document.write("<p>" + str + "</p>");
        }
    }
    else
    {
        document.write("Invalid Value");
    }
}

// Exercise 5
function numberTable()
{
    document.write("<table border='1'>");

    document.write("<table border='1'><tr>");

    for(let i = 1; i <= 20; i++)
    {
        if(i%2 ==0)
            document.write("<td style='color: blue'>" + i + "</td>");
        else
            document.write("<td style='color: red'>" + i + "</td>");

        if(i%5 == 0)
            document.write("</tr><tr>");
    }

    document.write("</table>")
}


// Exercise 6
function printVertical()
{
    const str = prompt("Text to print");

    for(let i = 0; i < str.length; i++)
    {
        document.write(str[i] + "<br/>");
    }
}

// Exercise 7
function printThanos()
{
    const str = prompt("Text to print");

    for(let i = 0; i < str.length; i++)
    {
        document.write(str.substr(0,str.length - i) +  "<br/>");
    }
}
