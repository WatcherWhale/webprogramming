const style = [
    "color: #f00",
    "font-size: 20px"
].join(';');

// Exercise 1
console.log("%cExercise 1", style);


class Summer
{
    constructor()
    {
        this.sum = 0;
    }

    getCurrentSum()
    {
        return this.sum;
    }

    add(number)
    {
        this.sum += number;
    }
}

const s1 = new Summer();
const s2 = new Summer();

s1.add(10);
s1.add(20);
s2.add(30);
s2.add(5);

// prints 30
console.log(s1.getCurrentSum());
// prints 35
console.log(s2.getCurrentSum());



// Exercise 2

console.log("%cExercise 2", style);

class Screen {

    constructor()
    {
        this._width = 0;
        this._height = 0;
    }

    diagonal()
    {
        return Math.sqrt(Math.pow(this.width, 2) + Math.pow(this.height, 2));
    }

    dimensions(definition)
    {
        let dimensions = definition.split('x')
        this.width = parseInt(dimensions[0]);
        this.height = parseInt(dimensions[1]);
        console.log(this.height + "x" + this.width)
    }

    get width()
    {
        return this._width;
    }

    set width(w)
    {
        this._width = w;
    }

    get height()
    {
        return this._height;
    }

    set height(h)
    {
        this._height = h;
    }
}

let screen = new Screen(0, 0);
screen.dimensions('500x300');
screen.width = 400;

console.log(screen.diagonal()); // Should print 500


// Exercise 3

console.log("%cExercise 3", style);

function sortByPriceAscending(jsonStr)
{
    const json = JSON.parse(jsonStr);

    return json.sort((a, b) => a.price - b.price + a.name.localeCompare(b.name));
}

console.log(sortByPriceAscending('[{"name":"eggs","price":1},{"name":"coffee","price":9.99},{"name":"rice","price":4.04},{"name":"abc","price":4.04}]'));


module.exports = {"Summer": Summer, "Screen": Screen, "sortByPriceAscending": sortByPriceAscending}
