# Labo 4
## 1
Implementeer een Summer object met 2 methoden: getCurrentSum() en add(getal). Daarna zorg je ervoor dat onderstaande code werkt:

```js
function Summer(){

}

const s1 = new Summer();
const s2 = new Summer();

s1.add(10);
s1.add(20);
s2.add(30);
s2.add(5);

// prints 30
console.log(s1.getCurrentSum());
// prints 35
console.log(s2.getCurrentSum());
```

Herschrijf de code als object literal en met de class definition

## 2

De code toont een API voor een JavaScript "Screen" klasse. Ik heb de methodes al geimplementeerd. Het is aan u om de constructor en accessors te maken zodat de uitvoering werkt:

```js
class Screen {

        diagonal()
        {
            return Math.sqrt(Math.pow(this.width, 2) + Math.pow(this.height, 2));
        }

        dimensions(definition)
        {
            let dimensions = definition.split('x')
            this.width = parseInt(dimensions[0]);
            this.height = parseInt(dimensions[1]);
            console.log(this.height + "x" + this.width)
        }
}

//UITVOERING
let screen = new Screen(0, 0);
screen.dimensions('500x300');
screen.width = 400;

console.log(screen.diagonal()); // Should print 500
```

## 3.  Moeilijke oefening !
Implementeer de functie sortByPriceAscending.
Deze functie accepteert een string in JSON formaat (object literals). Bekijk even deze link om een JSON string om te zetten naar objecten:
[How to convert a JSON string into a JavaScript object? (tutorialspoint.com)](https://www.tutorialspoint.com/how-to-convert-a-json-string-into-a-javascript-object)

Deze functie moet de lijst ordenen op prijs in stijgende volgorde. Indien 2 producten dezelfde prijs hebben moeten ze in alfabetische volgorde per naam staan.
Dus:

```js
sortByPriceAscending('[{"name":"eggs","price":1},{"name":"coffee","price":9.99},{"name":"rice","price":4.04}]');
```

geeft dit terug:

```js
'[{"name":"eggs","price":1},{"name":"rice","price":4.04},{"name":"coffee","price":9.99}]'
```

In code ziet dit er als volgt uit:
```js
console.log(sortByPriceAscending('[{"name":"eggs","price":1},{"name":"coffee","price":9.99},{"name":"rice","price":4.04},{"name":"abc","price":4.04}]'));
```

Je taak: maak de functie sortByPriceAscending.

(Deze link kan je ook verder helpen: [Array.prototype.sort() - JavaScript | MDN (mozilla.org](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort))
