const QUnit = require('qunit')
const exercises = require('../exercises');
//import { Summer, Screen, sortByPriceAscending } from '../exercises.js'
const Summer = exercises.Summer;

QUnit.test("Exercise 1", assert => {
    const s1 = new Summer();
    const s2 = new Summer();

    s1.add(10);
    s1.add(20);
    s2.add(30);
    s2.add(5);

    assert.equal(s1.getCurrentSum(), 30);
    assert.equal(s2.getCurrentSum(), 35);
});

QUnit.test("Exercise 2", assert => {
    const screen = new exercises.Screen(0, 0);
    screen.dimensions('500x300');
    screen.width = 400;

    assert.equal(screen.diagonal(), 500)
});


QUnit.test("Exercise 3", assert => {

    const expected = [
        { name: 'eggs', price: 1 },
        { name: 'abc', price: 4.04 },
        { name: 'rice', price: 4.04 },
        { name: 'coffee', price: 9.99 }
    ];
    const result = exercises.sortByPriceAscending('[{"name":"eggs","price":1},{"name":"coffee","price":9.99},{"name":"rice","price":4.04},{"name":"abc","price":4.04}]');

    assert.equal(JSON.stringify(result), JSON.stringify(expected));
});
